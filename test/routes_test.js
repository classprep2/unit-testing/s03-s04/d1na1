const chai = require('chai');
//  Sinced we installed chai, we could access it's contents and features through require
const expect = chai.expect;

//same as const { expect } = require("chai")

const http = require('chai-http'); 
chai.use(http);

describe("api_test_suite", () => {
	it("test_api_get_people_is_running", () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_get_people_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('test_api_post_person_returns_400_if_no_person_name', (done) => {		
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
		    alias: "Jason",
	      	age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	//  Activity
	it("test_api_get_person_is_running", () => {
		chai.request('http://localhost:5001').get('/users')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})


	it('test_api_post_returns_400_if_no_ALIAS', (done) => {		
		chai.request('http://localhost:5001')
		.post('/users')
		.type('json')
		.send({
		    name: "admin",
	      	age: 24
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_returns_400_if_no_AGE', (done) => {		
		chai.request('http://localhost:5001')
		.post('/users')
		.type('json')
		.send({
		    name: "admin",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	// Activity s04
	it('test_api_post_returns_400_if_no_username', (done) => {		
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
		    name: "admin@mail.com",
	      	password: "admin1234"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_returns_400_if_no_password', (done) => {		
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
		    username: "admin@mail.com",
		})

		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();	
		})		
	})

	it('test_api_post_returns_200_if_correct_credentials', (done) => {		
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
		    username: "admin@mail.com",
		    password: "admin1234"
		})

		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})


	it('stretch_goal_post_login_returns_403_if_wrong_credentials', (done) => {		
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
		    username: "admin5@mail.com",
		    password: "password12345"
		})

		.end((err, res) => {
			expect(res.status).to.equal(403);
			done();	
		})		
	})



 
})
