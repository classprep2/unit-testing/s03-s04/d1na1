/* This is where we create routes and response as accessing the endpoints */
/* Here we could use postman to test the routes */
const { names, aliases, accounts } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/people', (req, res) => {
		return res.send({
			people: names
		});
	})

	/*
		app.post('/person', (req, res) => {
			if(!req.body.hasOwnProperty('name')){		
				return res.status(400).send({
					'error': 'Bad Request - missing required parameter NAME'
				})
			}
			if(typeof req.body.name !== 'string'){
				return res.status(400).send({
					'error': 'Bad Request - NAME has to be a string'
				})
			}
			if(!req.body.hasOwnProperty('age')){
				return res.status(400).send({
					'error': 'Bad Request - missing required parameter AGE'
				})
			}
			if(typeof req.body.age !== "number"){
				return res.status(400).send({
					'error': 'Bad Request - AGE has to be a number'
				})	
			}
		})
	*/


	/* My own version of the condition */
	/* I also change some if same statments to else if and added else statement if name and age is inclded and has right inputs */
	app.post('/person', (req, res) => {
		if(!req.body.hasOwnProperty('name')){		
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}
		else if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - NAME has to be a string'
			})
		}
		else if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter AGE'
			})
		}
		else if(typeof req.body.age !== "number"){
			return res.status(400).send({
				'error': 'Bad Request - AGE has to be a number'
			})	
		}
			/*else{
				return res.status(200).send({
					'success' : 'Success, name and age included'
				})
			}*/

		})

	/* Activity */
	app.get('/users', (req, res) => {
		return res.send({
			users: aliases
		});
	})

	app.post('/users', (req, res) => {
		if(!req.body.hasOwnProperty('alias')){		
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter ALIAS'
			})
		}
		// Check if the post endpoint encounters an error if there is no age
		if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter AGE'
			})
		}
	})

	app.post('/login' , (req, res)=>{
		if(!req.body.hasOwnProperty('username')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter username'
			})
		}
		else if(!req.body.hasOwnProperty('password')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter password'
			})
		}


		
		isAccountExisting = false;
		accounts.find((account)=>{
			if(account.username == req.body.username && account.password == req.body.password){
				isAccountExisting = true;
			}
		});

		// correct credentials
		if(isAccountExisting == true){
			return res.status(200).send({
				'success': 'Thank you for logging in.'
			})
		}

		
		if(isAccountExisting == false){
			return res.status(403).send({
				'error': 'Wrong credentials.'
			})

		} 





	})
}
