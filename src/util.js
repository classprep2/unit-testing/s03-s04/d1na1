/* Creating an object */

const names = {
	"Brandon": {
		"name" : "Brandon Boyd",
		"age" : 35
	}, 
	"Steve" : {
		"name" : "Steve Tyler",
		"age" : 56
	}
}


const aliases = {
	"Brandon Boyd" : {
		"alias" : "brand",
		"age" : 35
	},
	"Steve Tyler" : {
		"alias" : "stevey",
		"age" : 56
	}
}

const accounts = [
	{
		"username" : "admin@mail.com",
		"password" : "admin1234"
	},
	{
		"username" : "guest@mail.com",
		"password" : "guest1234"
	}
]

function factorial(n){
	
	// Problem presented solution:
	if(n < 0) return undefined;

	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}


module.exports = {
	/* Here we export our function or object or even variables, in our case we are exporting a object*/
	names: names,
	aliases: aliases,
	accounts: accounts,
	factorial: factorial
}
 